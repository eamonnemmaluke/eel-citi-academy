package com.citi.training.trader.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.TwoMovingAveragesDao;
import com.citi.training.trader.model.Price;
import com.citi.training.trader.model.Stock;
import com.citi.training.trader.model.TwoMovingAveragesStrategy;

@Component
public class TwoMovingAverageService  {

	@Autowired
    private TwoMovingAveragesDao twoMovingAveragesDao;

    public List<TwoMovingAveragesStrategy> findAll(){
        return twoMovingAveragesDao.findAll();
    }

    public int save(TwoMovingAveragesStrategy strategy) {
        return twoMovingAveragesDao.save(strategy);
    }
    


}
