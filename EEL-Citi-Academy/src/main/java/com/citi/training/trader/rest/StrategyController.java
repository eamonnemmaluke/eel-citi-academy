package com.citi.training.trader.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.trader.model.Trade;
import com.citi.training.trader.model.TwoMovingAveragesStrategy;
import com.citi.training.trader.service.TradeService;
import com.citi.training.trader.service.TwoMovingAverageService;


@CrossOrigin(origins = "${com.citi.training.trader.cross-origin-host:http://localhost:4200}")
@RestController
@RequestMapping("${com.citi.training.trader.rest.trade-base-path:/strategyinput}")
public class StrategyController {
	
	
	private static final Logger logger =
            LoggerFactory.getLogger(StrategyController.class);
	
	@Autowired
    private TwoMovingAverageService stratService;
	
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<TwoMovingAveragesStrategy> findAll() {
		return stratService.findAll();
	}

	@RequestMapping(method=RequestMethod.POST,
            consumes=MediaType.APPLICATION_JSON_VALUE,
            produces=MediaType.APPLICATION_JSON_VALUE)
	public HttpEntity<TwoMovingAveragesStrategy> create(@RequestBody TwoMovingAveragesStrategy strate) {
		strate.setId(stratService.save(strate));
		
		return new ResponseEntity<TwoMovingAveragesStrategy>(strate, HttpStatus.CREATED);
	}
	
	

}
