package com.citi.training.trader.strategy;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.citi.training.trader.exceptions.TradeNotFoundException;
import com.citi.training.trader.messaging.TradeSender;
import com.citi.training.trader.model.Price;
import com.citi.training.trader.model.Trade;
import com.citi.training.trader.model.Trade.TradeState;
import com.citi.training.trader.model.TwoMovingAveragesStrategy;
import com.citi.training.trader.service.PriceService;
import com.citi.training.trader.service.TradeService;
import com.citi.training.trader.service.TwoMovingAverageService;

@Profile("!no-scheduled")
@Component
public class TwoMovingAveragesStrategyAlgorithm implements StrategyAlgorithm {

	private static final Logger logger = LoggerFactory.getLogger(TwoMovingAveragesStrategyAlgorithm.class);

	@Autowired
	private TradeSender tradeSender;

	@Autowired
	private PriceService priceService;

	@Autowired
	private TwoMovingAverageService strategyService;

	@Autowired
	private TradeService tradeService;

	@Scheduled(fixedRateString = "${simple.strategy.refresh.rate_ms:15000}")
	public void run() {

		for (TwoMovingAveragesStrategy strategy : strategyService.findAll()) {

			Trade lastTrade = null;
			try {
				lastTrade = tradeService.findLatestByStrategyId(strategy.getId());
			} catch (TradeNotFoundException ex) {
				logger.debug("No Trades for strategy id: " + strategy.getId());
			}

			if (lastTrade != null) {
				if (lastTrade.getState() == TradeState.WAITING_FOR_REPLY) {
					logger.debug("Waiting for last trade to complete, do nothing: [" + strategy.getId() + "]");
					continue;
				}
				if (!lastTrade.getAccountedFor() && lastTrade.getState() == TradeState.REJECTED) {
					logger.debug("The trade has been rejected, reattempting trade" + strategy.getId());
				}
				if (!lastTrade.getAccountedFor() && lastTrade.getState() == TradeState.FILLED) {
					logger.debug("Accounting for Trade  " + lastTrade);
					if (!strategy.hasPosition()) {
						if (lastTrade.getTradeType() == Trade.TradeType.SELL) {
							logger.debug("Confirmed short position for strategy: " + strategy);
							strategy.takeShortPosition();
						} else {
							logger.debug("Confirmed long position for strategy: " + strategy);
							strategy.takeLongPosition();
						}
						strategy.setLastTradePrice(lastTrade.getPrice());
					} else if (strategy.hasLongPosition()) {
						
					
						    logger.debug("Closing long position for strategy: " + strategy);
							// ?profit
							closePosition((lastTrade.getPrice() * lastTrade.getSize()) - (strategy.getLastTradePrice() * strategy.getSize()), strategy);
					
						
						
						
					} else if (strategy.hasShortPosition()) {
						
						    
						    logger.debug("Closing short position for strategy: " + strategy);
							// ?profit
							closePosition(((strategy.getLastTradePrice() * strategy.getSize()) - (lastTrade.getPrice() * lastTrade.getSize())) , strategy);
						
					}

				}
				
					lastTrade.setAccountedFor(true);
					tradeService.save(lastTrade);
				}

			if (strategy.getStopped() != null) {
				continue;
			}

			logger.debug("Taking strategic action");
			if (!strategy.hasPosition()) {

//				List<Price> prices = priceService.findLatest(strategy.getStock(), 2);
//
//				if (prices.size() < 2) {
//					logger.warn("Unable to execute strategy, not enough price data: " + strategy);
//					continue;
//				}
//				logger.debug("Taking position based on prices:");
//				for (Price price : prices) {
//					logger.debug(price.toString());
//				}
//
//				// if price going down => take short => sell
//				double currentPriceChange = prices.get(0).getPrice() - prices.get(1).getPrice();
//				logger.debug("Current Price change: " + currentPriceChange);
//
//				if (currentPriceChange < 0.001 && currentPriceChange > -0.001) {
//					logger.debug("Insufficient price change, taking no action");
//					continue;
//				}

				// if price going down => take short => sell
				// String currentPriceChange = strat.get(0).getTrigger();
				String currentPriceChangeTrigger = strategy.getCrossing();

				//logger.debug("Current Price change: " + currentPriceChange);

				if (currentPriceChangeTrigger.equals("BELOW")) {// && currentPriceChange < 0) {
					logger.debug("Trading to open short position for strategy: " + strategy);
					makeTrade(strategy, Trade.TradeType.SELL);
				} else {
					// if price going up => take long => buy
					logger.debug("Trading to open long position for strategy: " + strategy);
					makeTrade(strategy, Trade.TradeType.BUY);
				}
			} else if (strategy.hasLongPosition()) {
				logger.debug("Closing long position for strategy: " + strategy);
				makeTrade(strategy, Trade.TradeType.SELL);
			} else if (strategy.hasShortPosition()) {
				logger.debug("Closing short position for strategy: " + strategy);
				makeTrade(strategy, Trade.TradeType.BUY);
			}

			strategyService.save(strategy);

		}

	}

	private void closePosition(double price, TwoMovingAveragesStrategy strategy) {
		logger.debug("Recording profit/loss of: " + price + " for strategy: " + strategy);
		
		strategy.addProfitLoss(price);
		
		strategy.closePosition();
		

		if (strategy.getProfit() >= strategy.getClosingPosition()
		        || strategy.getProfit() <= 0 - (strategy.getClosingPosition())) {

			logger.debug("Exit condition reached, exiting strategy");
			
	
			strategy.setStopped(new Date());
			strategyService.save(strategy);
			
			
		}
	}

	private double makeTrade(TwoMovingAveragesStrategy strategy, Trade.TradeType tradeType) {
		Price currentPrice = priceService.findLatest(strategy.getStock(), 1).get(0);
		tradeSender.sendTrade(new Trade(currentPrice.getPrice(), strategy.getSize(), tradeType, strategy));
		return currentPrice.getPrice();
	}

}
