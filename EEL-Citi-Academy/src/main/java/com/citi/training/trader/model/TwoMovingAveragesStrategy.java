package com.citi.training.trader.model;

import java.util.Date;

public class TwoMovingAveragesStrategy {


	private static final int DEFAULT_MAX_TRADES = 20;

	    private int id;
	    private Stock stock;
	    private int size;
	    private int currentPosition;
	    private int closingPosition;
	    private Date stopped;
	    private double profit;
	    private double lastTradePrice;
	    private String crossing;
	    
	    public TwoMovingAveragesStrategy() {}
	    
	    public TwoMovingAveragesStrategy(Stock stock, int size) {
	        this(-1, stock, size, DEFAULT_MAX_TRADES, 0,0.0,null,0.0,"");
	    }
	    
	    public TwoMovingAveragesStrategy(int id, Stock stock, int size, Date stopped,
	    		int closingPosition, int currentPosition) {
	    	
	    		this.id = id;
		        this.stock = stock;
		        this.size = size;
		        this.stopped = stopped;
		        this.closingPosition = closingPosition;
		        this.currentPosition = currentPosition;
		        
	    }


	public TwoMovingAveragesStrategy(int id, Stock stock, int size, int currentPosition, int closingPosition,
	        double profit, Date stopped, double lastTradePrice, String crossing) {

			this.id = id;
			this.stock = stock;
			this.size = size;
			this.currentPosition = currentPosition;
			this.closingPosition = closingPosition;
			this.stopped = stopped;
			this.lastTradePrice = lastTradePrice;
			this.crossing = crossing;

		}

		public int getId() {
	        return id;
	    }

	    public void setId(int id) {
	        this.id = id;
	    }

	    public Stock getStock() {
	        return stock;
	    }

	    public void setStock(Stock stock) {
	        this.stock = stock;
	    }

	    public int getClosingPosition() {
			return closingPosition;
		}

		public void setClosingPosition(int closingPosition) {
			this.closingPosition = closingPosition;
		}


		public int getSize() {
	        return size;
	    }

	    public void setSize(int size) {
	        this.size = size;
	    }

	  

	    public int getCurrentPosition() {
	        return currentPosition;
	    }

	    public void setCurrentPosition(int currentPosition) {
	        this.currentPosition = currentPosition;
	    }

	    



		public Date getStopped() {
	        return stopped;
	    }

	    public void setStopped(Date stopped) {
	        this.stopped = stopped;
	    }
	    

		public double getLastTradePrice() {
			return lastTradePrice;
		}

		public void setLastTradePrice(double lastTradePrice) {
			this.lastTradePrice = lastTradePrice;
		}


	    public void stop() {
	        this.stopped = new Date();
	    }

	    public boolean hasPosition() {
	        return this.currentPosition != 0;
	    }

	    public boolean hasShortPosition() {
	        return this.currentPosition < 0;
	    }

	    public boolean hasLongPosition() {
	        return this.currentPosition > 0;
	    }

	    public void takeShortPosition() {
	        this.currentPosition = -1;
	    }

	    public void takeLongPosition() {
	        this.currentPosition = 1;
	    }

	    public void closePosition() {
	        this.currentPosition = 0;
	    }
	    
	    public double getProfit() {
			return profit;
		}

		public void setProfit(double profit) {
			this.profit = profit;
		}

		public void addProfitLoss(double profitLoss) {
	        this.profit = this.getProfit() + profitLoss;
	        
	    }

		public String getCrossing() {
			return crossing;
		}

		public void setCrossing(String crossing) {
			this.crossing = crossing;
		}

		@Override
		public String toString() {
			return "TwoMovingAveragesStrategy [id=" + id + ", stock=" + stock + ", size=" + size + ", currentPosition="
			        + currentPosition + ", closingPosition=" + closingPosition + ", stopped=" + stopped + ", profit="
			        + profit + ", lastTradePrice=" + lastTradePrice + ", crossing=" + crossing + "]";
		}
		
			

		
	

	  
	

	 
}
