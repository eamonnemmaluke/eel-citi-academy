package com.citi.training.trader.dao.mysql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.TwoMovingAveragesDao;
import com.citi.training.trader.model.Stock;
import com.citi.training.trader.model.TwoMovingAveragesStrategy;

@Component
public class MysqlTwoMovingAveragesStrategyDao implements TwoMovingAveragesDao {

	private static final Logger logger = LoggerFactory.getLogger(TwoMovingAveragesDao.class);

	private static String FIND_ALL_SQL = "select twomovingaverages.id as strategy_id, movingaveragesview.stock as stock_ticker, stock_id, size, closingPosition,twomovingaverages.stopped, currentPosition, lastTradePrice, profit"
										+ ", movingaveragesview.trigger as crossing "
										+ "FROM twomovingaverages "
										+ "LEFT JOIN movingaveragesview ON" 
										+ " movingaveragesview.id = twomovingaverages.stock_id";

	private static String INSERT_SQL = "INSERT INTO twomovingaverages (stock_id, size, "
			+ "closingPosition, currentPosition, lastTradePrice) "
	        + "values (:stock_id, :size, :closingPosition, :currentPosition, :lastTradePrice)";

	private static String UPDATE_SQL = "UPDATE twomovingaverages set stock_id=:stock_id, size=:size, "
	        + "stopped=:stopped, closingPosition=:closingPosition, "
	        + "currentPosition=:currentPosition, lastTradePrice=:lastTradePrice,profit=:profit where id=:id";

	@Autowired
	private JdbcTemplate tpl;

	@Autowired
	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Override
	public List<TwoMovingAveragesStrategy> findAll() {
		logger.debug("findAll SQL: [" + FIND_ALL_SQL + "]");
		return tpl.query(FIND_ALL_SQL, new TwoMovingAveragesStrategyMapper());
	}

	@Override
	public int save(TwoMovingAveragesStrategy strategy) {
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();

        namedParameters.addValue("stock_id", strategy.getStock().getId());
        namedParameters.addValue("size", strategy.getSize());
        namedParameters.addValue("stopped", strategy.getStopped());
        namedParameters.addValue("closingPosition", strategy.getClosingPosition());
        namedParameters.addValue("currentPosition", strategy.getCurrentPosition());
        namedParameters.addValue("lastTradePrice", strategy.getLastTradePrice());
        namedParameters.addValue("profit", strategy.getProfit());

        if(strategy.getId() <= 0) {
            logger.debug("Inserting TwoMovingAveragesStrategy: " + strategy);

            KeyHolder keyHolder = new GeneratedKeyHolder();

            namedParameterJdbcTemplate.update(INSERT_SQL, namedParameters, keyHolder);
            strategy.setId(keyHolder.getKey().intValue());
        } else {
            logger.debug("Updating TwoMovingAveragesStrategy: " + strategy);
            namedParameters.addValue("id", strategy.getId());
            namedParameterJdbcTemplate.update(UPDATE_SQL, namedParameters);
        }

        logger.debug("Saved trade: " + strategy);
        return strategy.getId();     
    }
	
	private static final class TwoMovingAveragesStrategyMapper implements RowMapper<TwoMovingAveragesStrategy> {
        public TwoMovingAveragesStrategy mapRow(ResultSet rs, int rowNum) throws SQLException {
            logger.debug("Mapping TwoMovingAveragesStrategy result set row num [" + rowNum + "], id : [" +
                         rs.getInt("strategy_id") + "]");

            return new TwoMovingAveragesStrategy(rs.getInt("strategy_id"),
                             new Stock(rs.getInt("stock_id"),
                            		 rs.getString("stock_ticker")),
                             rs.getInt("size"),
                             rs.getInt("currentPosition"),
                             rs.getInt("closingPosition"),
                             rs.getDouble("profit"),
                             rs.getDate("stopped"),
                             rs.getDouble("lastTradePrice"),
                             rs.getString("crossing"));
        }
    }



}
