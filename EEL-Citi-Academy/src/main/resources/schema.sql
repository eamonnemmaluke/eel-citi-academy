CREATE TABLE IF NOT EXISTS stock
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `ticker` VARCHAR(8) NOT NULL,
  UNIQUE(`ticker`),
  PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `twomovingaverages`
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `stock_id` INT NOT NULL,
  `size` INT NOT NULL,
  `stopped` DATETIME,
  `closingPosition` DOUBLE NOT NULL,
  `current_position` INT DEFAULT 0,
  `lastTradePrice` DOUBLE,
  `profit` DOUBLE,
  CONSTRAINT `strategy_stock_foreign_key`
    FOREIGN KEY (`stock_id`) REFERENCES stock (`id`),
  PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `trade`
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `strategy_id` INT NOT NULL,
  `price` DOUBLE NOT NULL,
  `size` INT NOT NULL,
  `buy` BOOLEAN NOT NULL,
  `state` VARCHAR(20),
  `last_state_change` DATETIME NOT NULL,
  `accounted_for` INT DEFAULT 0,
  `created` DATETIME NOT NULL,
  CONSTRAINT `trade_strategy_foreign_key`
    FOREIGN KEY (`strategy_id`) REFERENCES twomovingaverages (`id`),
  PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS price
(
  `id` INT NOT NULL AUTO_INCREMENT,
  `stock_id` INT NOT NULL,
  `price` DOUBLE,
  `recorded_at` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `price_stock`
    FOREIGN KEY (`stock_id`) REFERENCES stock (`id`)
);

CREATE VIEW `longavg` AS
(
    SELECT 
        AVG(`price`.`price`) AS `MA4D`,
        `stock`.`ticker` AS `Stock`,
        `stock`.`id` AS `id`
    FROM
        (`price`
        LEFT JOIN `stock` ON ((`stock`.`id` = `price`.`stock_id`)))
    WHERE
        (`price`.`recorded_at` >= (NOW() + INTERVAL -(4) HOUR))
    GROUP BY `stock`.`ticker`
);

CREATE VIEW `shortavg` AS
(
    SELECT 
        AVG(`price`.`price`) AS `MA3D`,
        `stock`.`ticker` AS `Stock`,
        `stock`.`id` AS `id`
    FROM
        (`price`
        LEFT JOIN `stock` ON ((`stock`.`id` = `price`.`stock_id`)))
    WHERE
        (`price`.`recorded_at` >= (NOW() + INTERVAL -(15) MINUTE))
    GROUP BY `stock`.`ticker`
);

CREATE VIEW `movingaveragesview` AS
(
    SELECT 
        `longavg`.`id` AS `id`
        `longavg`.`Stock` AS `stock`,
        `shortavg`.`MA3D` AS `MA3D`,
        `longavg`.`MA4D` AS `MA4D`,
        (`longavg`.`MA4D` - `shortavg`.`MA3D`) AS `Percentage`,
        (CASE
            WHEN (`shortavg`.`MA3D` < `longavg`.`MA4D`) THEN 'OVER'
            ELSE 'BELOW'
        END) AS `trigger`
    FROM
        (`longavg`
        LEFT JOIN `shortavg` ON ((`longavg`.`Stock` = `shortavg`.`Stock`)))
);